# The Into Film Signature Generator

The Into Film Signature Generator allows developers to create unique signature for any arrays and to be sure it is always the same for the same arrays. It doesn't depend on elements or keys order.

# Installation

## Requirements PHP

1. PHP 7+ compiled with the cURL extension
2. A recent version of cURL 7.16.2+ compiled with OpenSSL and zlib

## Composer

The recommended way to install the Into Film SDK is via [Composer](http://getcomposer.org). Composer is a tool for dependency management in PHP. It allows you to declare the dependent libraries your project needs and it will install them in your project for you.

```sh
# Install Composer
curl -sS https://getcomposer.org/installer | php
```

After installing you will need to add the Into Film Signature to the `composer.json`.
Due to this it is currently necessary to include a `repositories` section in the `composer.json`.

### Example composer.json

```json
{
  "repositories": [
    {
      "type": "git",
      "url": "ssh://git@bitbucket.org/intofilm/intofilm-signature.git"
    }
  ],
  "require": {
    "intofilm/intofilm-signature": "0.1.*"
  }
}
```

### Install dependencies

```sh
php composer.phar install
```

## Examples

### Using array input

```php
<?php
require_once 'vendor/autoload.php';

use IntoFilm\Signature\Signature;

$input = ['key1'=>'value1', 'key2'=>'value2'];
$generator = new Signature();
$signature = $generator->generate($input);
```

### Using json encoded string input

```php
<?php
require_once 'vendor/autoload.php';

use IntoFilm\Signature\Signature;

$input = '{"key1":"value1","key2":"value2"}';
$generator = new Signature();
$signature = $generator->generate($input);
```

## In Node.js

Using npm:

```shell
$ npm i @intofilm/signature
```

### Example

In Node.js:

```js
// Load the lib.
const signature = require("@intofilm/signature");

const hash = signature({ key1: "value1", key2: "value2" });
```
