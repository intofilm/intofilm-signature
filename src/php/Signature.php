<?php

namespace IntoFilm\Signature;

use IntoFilm\Signature\Exception\BadInputException;

/**
 * Class Signature
 * @package IntoFilm\Signature
 */
class Signature
{
    /**
     * @var Signature
     */
    protected static $instance;

    /**
     * Generate a signature from input array or json encoded string
     * @param string|array $input
     * @return string
     */
    public function generate($input)
    {
        $input = $this->getAsArray($input);
        $input = $this->sort($input);
        $input = json_encode($input);
        $result = $this->hash($input);

        return $result;
    }

    /**
     * Helper.
     *
     * @param array $input
     *
     * @return string
     */
    public static function create(array $input)
    {
        return self::getInstance()->generate($input);
    }

    /**
     * Returns input as array
     * Supports arrays and json encoded strings
     * @param array|string $input
     * @return mixed
     */
    protected function getAsArray($input)
    {
        if (true === is_string($input)) {
            $input = json_decode($input, true);

            if (JSON_ERROR_NONE !== json_last_error()) {
                throw new BadInputException('json decode failed, '.json_last_error_msg());
            }
        }

        if (false === is_array($input)) {
            throw new BadInputException('Input should be array or valid JSON encoded string');
        }

        return $input;
    }

    /**
     * Returns sorted array to always reproduce the same signature for equal data
     * @param array $input
     * @return array
     */
    protected function sort($input)
    {
        $doSort = function (&$input) use (&$doSort) {
            if (is_array($input)) {
                ksort($input);
                $input = array_map($doSort, $input);
            }

            return $input;
        };

        return $doSort($input);
    }

    /**
     * @return Signature
     */
    protected static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new static();
        }

        return self::$instance;
    }


    /**
     * Get hash of a message
     * @param string $input
     * @return string
     */
    public function hash($input)
    {
        return hash('sha512', $input);
    }
}