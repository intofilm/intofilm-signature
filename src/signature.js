const crypto = require("crypto");
const sort = require("sort-keys");

module.exports = input => {
  const data = JSON.stringify(sort(input, { deep: true }))
    // Replace unicode chars with their code point to match PHP
    .replace(
      /[^\x20-\x7F]/g,
      x => "\\u" + ("000" + x.codePointAt(0).toString(16)).slice(-4)
    )
    // Escape the slashes to match PHP JSON encoding.
    .replace(/\//g, "\\/");

  return crypto.createHash("sha512").update(data).digest("hex");
};
