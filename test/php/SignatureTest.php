<?php

namespace IntoFilm\Signature\Tests;

use IntoFilm\Signature\Exception\BadInputException;
use IntoFilm\Signature\Signature;
use PHPUnit\Framework\TestCase;

class SignatureTest extends TestCase
{
    /**
     * Test expected output is returned
     * @dataProvider getAsArrayCases()
     * @param $input
     * @param $expected
     */
    public function testGetAsArray($input, $expected)
    {
        $signature = new Signature();
        $result = $this->callMethod($signature, 'getAsArray', [$input]);

        $this->assertEquals($expected, $result);
    }

    /**
     * Test cases for testGetAsArray
     * @return array
     */
    public function getAsArrayCases()
    {
        return [
            [['foo' => 'bar', 'baz' => 'feed'], ['foo' => 'bar', 'baz' => 'feed']],
            ['{"foo":"bar", "baz":"feed"}', ['foo' => 'bar', 'baz' => 'feed']],
        ];
    }

    /**
     * Test exceptions are thrown on bad input
     * @dataProvider getAsArrayExceptionCases
     * @param $input
     */
    public function testGetAsArrayException($input)
    {
        $this->expectException(BadInputException::class);
        $signature = new Signature();
        $this->callMethod($signature, 'getAsArray', [$input]);
    }

    /**
     * Test cases for testGetAsArrayException
     * @return array
     */
    public function getAsArrayExceptionCases()
    {
        return [
            ['{"foo":"bar" "baz":"feed"}'],
            [null],
            [true],
            [5],
            ['OK'],
            [new \stdClass()],
        ];
    }

    /**
     * @dataProvider getSortCases()
     * @param $input
     * @param $expected
     */
    public function testSort($input, $expected)
    {
        $signature = new Signature();
        $result = $this->callMethod($signature, 'sort', [$input]);

        $this->assertEquals(json_encode($expected), json_encode($result));
    }

    /**
     * Test cases for testSort
     * @return array
     */
    public function getSortCases()
    {
        $res = [
            'First' => 1,
            'be second' => 2,
            'end' => [
                'arr' => ['a' => 'b', 'c' => 'd', 'e' => 'f'],
                'baz' => 'feed',
                'foo' => 'bar',
            ],
        ];

        return [
            [$res, $res],
            [
                [
                    'be second' => 2,
                    'end' => [
                        'foo' => 'bar',
                        'arr' => ['c' => 'd', 'e' => 'f', 'a' => 'b',],
                        'baz' => 'feed',
                    ],
                    'First' => 1,
                ],
                $res,
            ],
        ];
    }

    /**
     * Test hash is generated from string
     */
    public function testHash()
    {
        $input = '{"baz":"feed","foo":"bar"}';
        $expected = "0586c966ea348e7a8e491c01b00fe8e72d94b2c77ade45017e6b555fff493906e626f40489c50efcff8a3c58e0146455956cf1a5fc5e400fbf7ad862aae1ac0a";
        $signature = new Signature();
        $result = $this->callMethod($signature, 'hash', [$input]);

        $this->assertEquals($expected, $result);
    }

    /**
     * Test that signature is generated from array and json encoded string
     * Checks that key order is insignificant
     */
    public function testGenerate()
    {
        $input1 = ['foo' => 'bar', 'baz' => 'feed'];
        $input2 = ['baz' => 'feed', 'foo' => 'bar'];
        $input3 = '{"baz":"feed","foo":"bar"}';
        $input4 = ['foo' => 'feed', 'baz' => 'bar'];
        $expected = "0586c966ea348e7a8e491c01b00fe8e72d94b2c77ade45017e6b555fff493906e626f40489c50efcff8a3c58e0146455956cf1a5fc5e400fbf7ad862aae1ac0a";
        $signature = new Signature();
        $result1 = $signature->generate($input1);
        $result2 = $signature->generate($input2);
        $result3 = $signature->generate($input3);
        $result4 = $signature->generate($input4);
        $this->assertEquals($expected, $result1);
        $this->assertEquals($expected, $result2);
        $this->assertEquals($expected, $result3);
        $this->assertNotEquals($expected, $result4);
    }

    /**
     * Call private or protected method
     * @param $object
     * @param $methodName
     * @param $arguments
     * @return mixed
     */
    protected function callMethod($object, $methodName, $arguments)
    {
        $method = new \ReflectionMethod($object, $methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $arguments);
    }

    /**
     * @param array $input
     * @param       $signature
     *
     * @dataProvider signatureProvider
     */
    public function testSignaturesMatch(array $input, $signature)
    {
        self::assertSame($signature, Signature::create($input));
    }

    public function signatureProvider()
    {
        $fixtures = [];

        $json = function($file) {
            return json_decode(file_get_contents(__DIR__  . '/../' . $file), true);
        };

        foreach ($json('fixtures.json') as $key => $value) {
            $fixtures[] = [$json($key), $value];
        }

        return $fixtures;
    }
}
