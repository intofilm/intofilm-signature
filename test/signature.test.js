const assert = require("assert");
const fixtures = require("./fixtures.json");
const signature = require("../src/signature");

describe("signature generation", function () {
  describe("grab a list of JSON files and check their signatures against a known value", function () {
    Object.keys(fixtures).forEach(filename => {
      const g = signature(require("./" + filename));
      it("generate the matching signature for " + filename, function () {
        assert.equal(
          g,
          fixtures[filename],
          "Matching signature for " + filename
        );
      });
    });
  });
});
